artifactory_url="http://34.72.195.8:8081/artifactory"

repo="libs-snapshot-local"

artifacts="com/qaagility"

app_name="HelloWorld"

url="$artifactory_url/$repo/$artifacts/$app_name"

file="curl -s $url/maven-metadata.xml"

version=`curl -u admin:Password@4505 -s "$url/maven-metadata.xml" | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`

build=`curl -u admin:Password@4505 -s "$url/$version/maven-metadata.xml" | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`

BUILD_LATEST="$url/$version/$app_name-$build.war"

echo $BUILD_LATEST > filename.txt
